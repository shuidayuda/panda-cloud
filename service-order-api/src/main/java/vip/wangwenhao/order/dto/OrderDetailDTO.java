package vip.wangwenhao.order.dto;


import lombok.Getter;
import lombok.Setter;

/**
 * @author wangwenhao
 */
@Getter
@Setter
public class OrderDetailDTO {
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private Long orderId;

	/**
	 *  
	 */
	private Long productId;

	/**
	 *  
	 */
	private Integer productNum;


}

