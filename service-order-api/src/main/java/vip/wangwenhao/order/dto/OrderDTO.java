package vip.wangwenhao.order.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author wangwenhao
 */

@Setter
@Getter
public class OrderDTO {
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private String orderNum;

	/**
	 *  
	 */
	private Long totalAmount;

	/**
	 *
	 */
	private Byte orderStatus;

	private List<OrderDetailDTO> detailList;

}

