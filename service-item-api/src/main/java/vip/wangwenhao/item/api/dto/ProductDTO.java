package vip.wangwenhao.item.api.dto;

import lombok.Getter;
import lombok.Setter;
import vip.wangwenhao.common.annotations.LockParam;

/**
 * @author wwh
 * @date 2019年12月12日 17:57
 */

@Getter
@Setter
public class ProductDTO {

    /**
     *
     */
    @LockParam("itemId")
    private Long id;

    /**
     *
     */
    @LockParam("productName")
    private String productName;

    /**
     *
     */
    private Long productPrice;

    /**
     *
     */
    private String productDesc;

    /**
     *
     */
    private String productImg;
    /**
     *
     */
    private Integer productStock;
}