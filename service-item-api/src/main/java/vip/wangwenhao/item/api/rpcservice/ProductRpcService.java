package vip.wangwenhao.item.api.rpcservice;

import org.springframework.cloud.openfeign.DefaultHystrixFallbackFactory;
import org.springframework.cloud.openfeign.FallbackEnableFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.item.api.dto.ProductDTO;

/**
 * @author wwh
 * @date 2019年12月12日 17:07
 */
@FeignClient(name = "service-item",fallbackFactory = DefaultHystrixFallbackFactory.class)
public interface ProductRpcService extends FallbackEnableFeignClient {

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "item/queryById", method = RequestMethod.GET)
    Result<ProductDTO> queryById(@RequestParam("id") Long id);

    /**
     * update
     *
     * @param productDTO
     * @return
     */
    @RequestMapping(value = "item/update", method = RequestMethod.POST)
    Result<ProductDTO> update(@RequestBody ProductDTO productDTO);

    /**
     * insert
     *
     * @param productDTO
     * @return
     */
    @RequestMapping(value = "item/insert", method = RequestMethod.POST)
    Result<ProductDTO> insert(@RequestBody ProductDTO productDTO);

    /**
     * 扣减库存
     * @param id
     * @param num
     * @return
     */
    @RequestMapping(value = "item/decrStock", method = RequestMethod.POST)
    Result<Integer> decrStock(@RequestParam("id") Long id, @RequestParam("num") Integer num);
}