package vip.wangwenhao.open;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.context.request.RequestContextListener;
import vip.wangwenhao.common.constants.CommonConstants;

/**
 * @author wangwenhao
 */
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "vip.wangwenhao.*.api.rpcservice")
@SpringBootApplication(scanBasePackages = {CommonConstants.BASE_PACKAGE}, exclude = {DataSourceAutoConfiguration.class})
@EnableRedisHttpSession
public class OpenApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(OpenApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(OpenApiApplication.class);
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

}