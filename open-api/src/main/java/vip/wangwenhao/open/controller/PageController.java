package vip.wangwenhao.open.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author wangwenhao
 */
@Controller
public class PageController {
    @GetMapping("/index")
    public String index() {
        return "index";
    }
  }