package vip.wangwenhao.open.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.item.api.dto.ProductDTO;
import vip.wangwenhao.item.api.rpcservice.ProductRpcService;

import javax.annotation.Resource;


/**
 * @author wwh
 * @date 2019年12月12日 18:31
 */

@RestController
@RequestMapping("item")
public class ItemController {

    @Resource
    private ProductRpcService productRpcService;

    @RequestMapping(value = "query")
    public Result<ProductDTO> queryById(@RequestParam("id") Long id) {
        return productRpcService.queryById(id);
    }

    @RequestMapping(value = "update")
    public Result<ProductDTO> update(@RequestBody ProductDTO productDTO) {
        return productRpcService.update(productDTO);
    }

    @RequestMapping(value = "decrStock")
    public Result<Integer> decrStock(@RequestParam("id") Long id,@RequestParam("num") Integer num) {
        return productRpcService.decrStock(id,num);
    }

}