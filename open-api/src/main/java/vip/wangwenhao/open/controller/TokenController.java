package vip.wangwenhao.open.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.autoconfigure.constants.Oauth2Constants;
import vip.wangwenhao.autoconfigure.utils.Oauth2Utils;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.sso.api.rpcservice.TokenRpcService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
*  
*
* @author wwh
* @date 2020年01月15日 14:19
* 
*/
@RestController
@RequestMapping("token")
public class TokenController {

    @Resource
    private TokenRpcService tokenRpcService;

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public Result<JSONObject> tokenLogin(HttpServletRequest request, @RequestParam(value = Oauth2Constants.ACCESS_TOKEN_QUERY_NAME,required = false) String accessToken) {
        String token = Oauth2Utils.getToken(request);
        if (StringUtils.isEmpty(token)){
            token = accessToken;
        }
        return tokenRpcService.login(token);
    }

    @RequestMapping(value = "read", method = RequestMethod.POST)
    public Result<JSONObject> read(@RequestParam(Oauth2Constants.ACCESS_TOKEN_QUERY_NAME) String accessToken) {
        return tokenRpcService.read(accessToken);
    }

}