package vip.wangwenhao.open.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.common.annotations.GroupResubmitLock;
import vip.wangwenhao.common.annotations.LimitLock;
import vip.wangwenhao.common.annotations.LocalResubmitLock;
import vip.wangwenhao.common.annotations.LockParam;
import vip.wangwenhao.common.exception.LimitException;
import vip.wangwenhao.item.api.dto.ProductDTO;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wwh
 * @date 2019年12月18日 20:46
 */

@RestController
@LocalResubmitLock("hello")
@RequestMapping("hello")
public class HelloWorldController {
    private static final AtomicInteger ATOMIC_INTEGER = new AtomicInteger();


    @LimitLock(period = 100, count = 10)
    @GetMapping("/limit")
    public int testLimiter() {
        if (ATOMIC_INTEGER.get() == 2) {
            throw new LimitException();
        }
        return ATOMIC_INTEGER.incrementAndGet();
    }

    @GetMapping("/local")
    public String local(@LockParam("param") @RequestParam String token) {
        return "success - " + token;
    }

    @GroupResubmitLock("world")
    @GetMapping("/group")
    public String group(@LockParam("pojo") ProductDTO productDTO, @LockParam @RequestParam String token) {
        return "success - " + token + JSONObject.toJSONString(productDTO, SerializerFeature.IgnoreNonFieldGetter);
    }

}