package vip.wangwenhao.open.controller;

import org.springframework.web.bind.annotation.*;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.mq.api.dto.Message;
import vip.wangwenhao.mq.api.rpcservice.WebsocketRpcService;

import javax.annotation.Resource;

/**
 * @author wwh
 * @date 2020年03月01日 16:35
 */

@RestController
@RequestMapping("mq")
public class MqController {

    @Resource
    private WebsocketRpcService websocketRpcService;

    @RequestMapping(value = "/topic", method = RequestMethod.POST)
    public Result<Message> sendToTopic(@RequestBody Message message) {
        return websocketRpcService.sendToTopic(message);
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Result<Message> sendToUser(@RequestBody Message message) {
        return websocketRpcService.sendToUser(message);
    }

}