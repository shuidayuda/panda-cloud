package vip.wangwenhao.open.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import vip.wangwenhao.mq.api.dto.Message;


@Component
@Slf4j
@RabbitListener(queues = "TestDirectQueue")
public class DirectReceiver {
 
    @RabbitHandler
    public void process(Message message) {

        log.info("receive message:{}",message);

    }
 
}
