package vip.wangwenhao.open;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.item.api.dto.ProductDTO;
import vip.wangwenhao.item.api.rpcservice.ProductRpcService;

import javax.annotation.Resource;

/**
 * @author wwh
 * @date 2019年12月12日 15:21
 */

@SpringBootTest(classes = OpenApiApplication.class)
@RunWith(SpringRunner.class)
public class BaseTest {


    @Resource
    private ProductRpcService productRpcService;

    @Test
    public void test() {
        Result<ProductDTO> itemDTOResult = productRpcService.queryById(1L);
        System.out.println(JSONObject.toJSONString(itemDTOResult, SerializerFeature.IgnoreNonFieldGetter));
    }

}