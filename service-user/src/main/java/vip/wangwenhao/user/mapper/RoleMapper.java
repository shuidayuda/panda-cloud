package vip.wangwenhao.user.mapper;


import vip.wangwenhao.user.entity.Role;

import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface RoleMapper {
    
    void insert(Role role);

    int update(Role role);

    List<Role> query(Role role);

    int delete(Long id);

    Role selectByPrimaryKey(Long id);

}
