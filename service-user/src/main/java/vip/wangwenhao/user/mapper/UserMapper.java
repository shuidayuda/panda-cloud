package vip.wangwenhao.user.mapper;

import vip.wangwenhao.user.entity.User;

import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface UserMapper {
    
    void insert(User user);

    int update(User user);

    List<User> query(User user);

    int delete(Long id);

    User selectByPrimaryKey(Long id);

    User selectByUsername(String username);

}
