package vip.wangwenhao.user.mapper;


import vip.wangwenhao.user.entity.RolePermission;

import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface RolePermissionMapper {
    
    void insert(RolePermission rolePermission);

    int update(RolePermission rolePermission);

    List<RolePermission> query(RolePermission rolePermission);

    int delete(Long permissionId, Long roleId);

    RolePermission selectByPrimaryKey(Long permissionId, Long roleId);

}
