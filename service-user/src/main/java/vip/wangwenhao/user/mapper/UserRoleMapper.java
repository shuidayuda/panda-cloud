package vip.wangwenhao.user.mapper;


import vip.wangwenhao.user.entity.UserRole;

import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface UserRoleMapper {
    
    void insert(UserRole userRole);

    int update(UserRole userRole);

    List<UserRole> query(UserRole userRole);

    int delete(Long roleId, Long userId);

    UserRole selectByPrimaryKey(Long roleId, Long userId);

}
