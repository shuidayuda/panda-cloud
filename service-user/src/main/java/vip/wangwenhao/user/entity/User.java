package vip.wangwenhao.user.entity;


import lombok.*;

/**
 * p_user 实体类
 * @author wangwenhao
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class User{
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private String username;

	/**
	 *  
	 */
	private String password;

}

