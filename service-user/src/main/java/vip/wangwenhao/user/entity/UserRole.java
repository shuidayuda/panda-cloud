package vip.wangwenhao.user.entity;


import lombok.*;

/**
 * p_user_role 实体类
 * @author wangwenhao
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class UserRole{
	/**
	 *  
	 */
	private Long userId;

	/**
	 *  
	 */
	private Long roleId;
}

