package vip.wangwenhao.user.entity;


import lombok.*;

/**
 * p_permission 实体类
 * @author wangwenhao
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Permission{
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private String permissionName;
}

