package vip.wangwenhao.user.entity;


import lombok.*;

/**
 * p_role_permission 实体类
 * @author wangwenhao
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class RolePermission{
	/**
	 *  
	 */
	private Long roleId;

	/**
	 *  
	 */
	private Long permissionId;

}

