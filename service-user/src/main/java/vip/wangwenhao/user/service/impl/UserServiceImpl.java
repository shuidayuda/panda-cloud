package vip.wangwenhao.user.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vip.wangwenhao.common.util.BeanUtils;
import vip.wangwenhao.common.enums.ResultCode;
import vip.wangwenhao.common.exception.CommonException;
import vip.wangwenhao.user.api.dto.UserDTO;
import vip.wangwenhao.user.entity.User;
import vip.wangwenhao.user.mapper.UserMapper;
import vip.wangwenhao.user.service.UserService;

import javax.annotation.Resource;

/**
 * @author wwh
 * @date 2019年12月29日 05:29
 */

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public UserDTO queryByUsername(String username) {
        try {
            User user = userMapper.selectByUsername(username);
            if (null != user) {
                return BeanUtils.copyTo(user, UserDTO.class);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("queryByUsername {}", username, e);
            throw new CommonException(ResultCode.DB_OPR_FAIL, "queryByUsername error");
        }
    }
}