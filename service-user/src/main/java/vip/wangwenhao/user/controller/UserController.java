package vip.wangwenhao.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.user.api.dto.UserDTO;
import vip.wangwenhao.user.service.UserService;

/**
 * @author wwh
 * @date 2019年12月25日 14:14
 */

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "get", method = RequestMethod.GET)
    public UserDTO currentUser(Authentication authentication) {
        String username;
        Object principal = authentication.getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User){
            username = ((User) principal).getUsername();
        }else {
            username = String.valueOf(principal);
        }
        UserDTO userDTO = userService.queryByUsername(username);
        return userDTO;
    }

}