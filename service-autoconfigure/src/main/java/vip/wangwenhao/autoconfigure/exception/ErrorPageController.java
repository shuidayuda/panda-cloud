package vip.wangwenhao.autoconfigure.exception;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*  
*
* @author wwh
* @date 2020年01月16日 23:54
* 
*/

@Controller
@RequestMapping("error")
public class ErrorPageController {

    @RequestMapping("/401")
    public String error401() {
        return "401";
    }

    @RequestMapping("/403")
    public String error403() {
        return "403";
    }

    @RequestMapping("/404")
    public String error404() {
        return "404";
    }

    @RequestMapping("/500")
    public String error500() {
        return "500";
    }

}