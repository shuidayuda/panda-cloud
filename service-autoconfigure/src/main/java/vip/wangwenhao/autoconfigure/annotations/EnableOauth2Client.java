package vip.wangwenhao.autoconfigure.annotations;

import org.springframework.context.annotation.Import;
import vip.wangwenhao.autoconfigure.oauth2.Oauth2ClientConfig;

import java.lang.annotation.*;

/**
 * @author wwh
 * @date 2020年01月19日 22:23
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@EnableOauth2ClientCustomException
@EnableErrorPage
@EnableSwaggerAndWebMvcConfig
@Import(Oauth2ClientConfig.class)
public @interface EnableOauth2Client {
}