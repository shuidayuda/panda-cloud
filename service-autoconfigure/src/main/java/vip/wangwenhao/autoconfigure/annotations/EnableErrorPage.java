package vip.wangwenhao.autoconfigure.annotations;

import org.springframework.context.annotation.Import;
import vip.wangwenhao.autoconfigure.exception.ErrorPageConfig;

import java.lang.annotation.*;

/**
 * @author wwh
 * @date 2020年01月19日 22:08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({ErrorPageConfig.class})
public @interface EnableErrorPage {
}