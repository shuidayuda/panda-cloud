package vip.wangwenhao.autoconfigure.annotations;

import org.springframework.context.annotation.Import;
import vip.wangwenhao.autoconfigure.oauth2.Oauth2ClientCustomExceptionConfig;

import java.lang.annotation.*;

/**
 * @author wwh
 * @date 2020年01月19日 22:20
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({Oauth2ClientCustomExceptionConfig.class})
public @interface EnableOauth2ClientCustomException {
}