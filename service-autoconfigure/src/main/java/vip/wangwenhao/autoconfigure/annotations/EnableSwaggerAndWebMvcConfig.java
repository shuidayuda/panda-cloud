package vip.wangwenhao.autoconfigure.annotations;

import org.springframework.context.annotation.Import;
import vip.wangwenhao.autoconfigure.base.Swagger2;
import vip.wangwenhao.autoconfigure.base.WebMvcConfig;

import java.lang.annotation.*;

/**
 * @author wwh
 * @date 2020年01月19日 22:08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({Swagger2.class, WebMvcConfig.class})
public @interface EnableSwaggerAndWebMvcConfig {
}