package vip.wangwenhao.autoconfigure.annotations;

import org.springframework.context.annotation.Import;
import vip.wangwenhao.autoconfigure.aspect.LimitAspect;

import java.lang.annotation.*;

/**
 * @author wwh
 * @date 2020年01月19日 22:08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({LimitAspect.class})
public @interface EnableLimit {
}