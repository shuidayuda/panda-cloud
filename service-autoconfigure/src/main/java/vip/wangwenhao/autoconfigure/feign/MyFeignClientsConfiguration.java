package vip.wangwenhao.autoconfigure.feign;

import feign.Feign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.openfeign.DefaultHystrixFallbackFactory;
import org.springframework.cloud.openfeign.HystrixFallbackConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

/**
 * @author wangwenhao
 */
@Configuration
@ConditionalOnClass(Feign.class)
@Import({HystrixFallbackConfiguration.class})
public class MyFeignClientsConfiguration {

    @Bean
    @Scope("prototype")
    public DefaultHystrixFallbackFactory defaultHystrixFallbackFactory() {
        DefaultHystrixFallbackFactory defaultHystrixFallbackFactory = new DefaultHystrixFallbackFactory();
        defaultHystrixFallbackFactory.setFallbackHandlerFactory(new MyHystrixFallbackHandlerFactory());
        return defaultHystrixFallbackFactory;
    }

}