package vip.wangwenhao.autoconfigure.feign;

import org.springframework.cloud.openfeign.HystrixFallbackHandler;
import org.springframework.cloud.openfeign.HystrixFallbackHandlerFactory;

/**
 * @author wangwenhao
 */
public class MyHystrixFallbackHandlerFactory implements HystrixFallbackHandlerFactory {

    @Override
    public HystrixFallbackHandler createHandler(Class<?> feignClientClass, Throwable cause) {
        return new MyHystrixFallbackHandler(feignClientClass, cause);
    }

}