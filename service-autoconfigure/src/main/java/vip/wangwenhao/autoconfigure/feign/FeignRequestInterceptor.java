package vip.wangwenhao.autoconfigure.feign;

import com.google.common.base.Strings;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import vip.wangwenhao.autoconfigure.constants.GatewayConstants;
import vip.wangwenhao.autoconfigure.constants.Oauth2Constants;
import vip.wangwenhao.autoconfigure.constants.SessionConstants;

import vip.wangwenhao.common.constants.CommonConstants;
import vip.wangwenhao.common.constants.SymbolConstants;
import vip.wangwenhao.common.util.EncryptUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * Feign调用的时候添加请求头from
 *
 * @author wangwenhao
 */
@Configuration
@Slf4j
public class FeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(GatewayConstants.GATEWAY_AUTH_HEADER, GatewayConstants.FROM_SECRET);
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attrs != null) {
            HttpServletRequest request = attrs.getRequest();

            Enumeration<String> headerNames = request.getHeaderNames();
            if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                    String name = headerNames.nextElement();
                    Enumeration<String> values = request.getHeaders(name);
                    while (values.hasMoreElements()) {
                        String value = values.nextElement();
                        requestTemplate.header(name, value);
                    }
                }
            }
            Cookie[] cookies = request.getCookies();
            StringBuffer stringBuffer = new StringBuffer();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    stringBuffer.append(cookie.getName());
                    stringBuffer.append(SymbolConstants.EQUAL);
                    stringBuffer.append(cookie.getValue());
                    stringBuffer.append(SymbolConstants.SEMICOLON);
                    stringBuffer.append(SymbolConstants.BLANK);
                    requestTemplate.header(cookie.getName(), cookie.getValue());
                }
            } else {
                String sessionId = attrs.getSessionId();
                if (!Strings.isNullOrEmpty(sessionId)) {
                    String cookie = SessionConstants.SESSION_COOKIE_KEY + SymbolConstants.EQUAL + EncryptUtils.base64Encode(sessionId) + SymbolConstants.SEMICOLON + SymbolConstants.BLANK;
                    requestTemplate.header(SessionConstants.COOKIE_HEADER, cookie);
                    requestTemplate.header(SessionConstants.SESSION_HEADER, sessionId);
                }
            }
            if (stringBuffer.length() > CommonConstants.TWO) {
                requestTemplate.header(SessionConstants.COOKIE_HEADER, stringBuffer.substring(0, stringBuffer.length() - 2));
            }
        }

        requestTemplate.header(GatewayConstants.GATEWAY_AUTH_HEADER, GatewayConstants.FROM_SECRET);
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //添加token
        String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (!StringUtils.isEmpty(authorization)) {
            requestTemplate.header(HttpHeaders.AUTHORIZATION, authorization);
        }
        String accessToken = request.getParameter(SessionConstants.TOKEN_SESSION_KEY);
        if (StringUtils.isNotBlank(accessToken)) {
            requestTemplate.header(HttpHeaders.AUTHORIZATION, Oauth2Constants.BEARER_AUTHENTICATION + accessToken);
        }
    }
}