package vip.wangwenhao.autoconfigure.feign;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.HystrixFallbackHandler;
import vip.wangwenhao.common.enums.ResultCode;
import vip.wangwenhao.common.util.ResultUtils;

import java.lang.reflect.Method;

/**
 * @author wangwenhao
 */
@Slf4j
public class MyHystrixFallbackHandler extends HystrixFallbackHandler {

    public MyHystrixFallbackHandler(Class<?> feignClientClass, Throwable cause) {
        super(feignClientClass, cause);
    }

    @Override
    protected Object handle(Object proxy, Method method, Object[] args) throws Throwable {
        String serviceName = method.getDeclaringClass().getName();
        String methodName = method.getName();
        String params = JSONArray.toJSONString(args, SerializerFeature.IgnoreNonFieldGetter);
        log.error("feign call error {}#{} params={}", serviceName, methodName, params);
        Class<?> returnType = method.getReturnType();
        return ResultUtils.result(returnType, ResultCode.RPC_SERVICE_EXEC_ERROR, String.format("feign call error %s#%s params=%s", serviceName, methodName, params));
    }
}