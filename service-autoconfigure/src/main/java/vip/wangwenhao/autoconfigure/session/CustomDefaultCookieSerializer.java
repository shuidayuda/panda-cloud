//package vip.wangwenhao.autoconfigure.session;
//
//import com.google.common.collect.Lists;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.session.web.http.DefaultCookieSerializer;
//import vip.wangwenhao.autoconfigure.constants.Oauth2Constants;
//import vip.wangwenhao.autoconfigure.constants.SessionConstants;
//import vip.wangwenhao.common.constants.CommonConstants;
//import vip.wangwenhao.common.constants.SymbolConstants;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import java.util.ArrayList;
//import java.util.Base64;
//import java.util.List;
//
///**
//*
//*
//* @author wwh
//* @date 2020年01月14日 18:02
//*
//*/
//
//public class CustomDefaultCookieSerializer extends DefaultCookieSerializer {
//
//    private String cookieName = "SESSION";
//
//    private boolean useBase64Encoding = true;
//
//    private String jvmRoute;
//
//    @Override
//    public List<String> readCookieValues(HttpServletRequest request) {
//        Cookie[] cookies = request.getCookies();
//        if (null == cookies){
//            String header = request.getHeader(SessionConstants.COOKIE_HEADER);
//            cookies = header2Cookie(header);
//        }
//        List<String> matchingCookieValues = new ArrayList();
//        if (cookies != null) {
//            Cookie[] var4 = cookies;
//            int var5 = cookies.length;
//
//            for(int var6 = 0; var6 < var5; ++var6) {
//                Cookie cookie = var4[var6];
//                if (this.cookieName.equals(cookie.getName())) {
//                    String sessionId = this.useBase64Encoding ? this.base64Decode(cookie.getValue()) : cookie.getValue();
//                    if (sessionId != null) {
//                        if (this.jvmRoute != null && sessionId.endsWith(this.jvmRoute)) {
//                            sessionId = sessionId.substring(0, sessionId.length() - this.jvmRoute.length());
//                        }
//
//                        matchingCookieValues.add(sessionId);
//                    }
//                }
//            }
//        }
//
//        return matchingCookieValues;
//    }
//
//    private String base64Decode(String base64Value) {
//        try {
//            byte[] decodedCookieBytes = Base64.getDecoder().decode(base64Value);
//            return new String(decodedCookieBytes);
//        } catch (Exception var3) {
//            return null;
//        }
//    }
//
//    private Cookie[] header2Cookie(String header){
//        if (StringUtils.isNotBlank(header)){
//            List<Cookie> cookies = Lists.newArrayList();
//            String[] split = header.split(SymbolConstants.SEMICOLON + SymbolConstants.BLANK);
//            if (null != split){
//                for (String s:split){
//                    String[] map = s.split(SymbolConstants.EQUAL);
//                    if (map.length == CommonConstants.TWO){
//                        Cookie cookie = new Cookie(map[CommonConstants.ZERO],map[CommonConstants.ONE]);
//                        cookies.add(cookie);
//                    }
//                }
//            }
//            return cookies.toArray(new Cookie[0]);
//        }
//        return null;
//    }
//}