//package vip.wangwenhao.autoconfigure.session;
//
//import org.springframework.session.web.http.CookieHttpSessionIdResolver;
//import org.springframework.session.web.http.CookieSerializer;
//import org.springframework.session.web.http.HttpSessionIdResolver;
//import org.springframework.util.CollectionUtils;
//import vip.wangwenhao.autoconfigure.constants.Oauth2Constants;
//import vip.wangwenhao.autoconfigure.constants.SessionConstants;
//import vip.wangwenhao.common.constants.CommonConstants;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Collections;
//import java.util.List;
//
///**
// * @author wwh
// * @date 2020年01月14日 16:44
// */
//
//public class CustomHttpSessionIdResolver implements HttpSessionIdResolver {
//    private static final String WRITTEN_SESSION_ID_ATTR = CookieHttpSessionIdResolver.class.getName().concat(".WRITTEN_SESSION_ID_ATTR");
//    private CookieSerializer cookieSerializer = new CustomDefaultCookieSerializer();
//
//    public CustomHttpSessionIdResolver() {
//    }
//
//    @Override
//    public List<String> resolveSessionIds(HttpServletRequest request) {
//        List<String> strings = this.cookieSerializer.readCookieValues(request);
//        if (CollectionUtils.isEmpty(strings)) {
//            String headerValue = request.getHeader(SessionConstants.SESSION_HEADER);
//            return headerValue != null ? Collections.singletonList(headerValue) : Collections.emptyList();
//        }
//        return strings;
//
//    }
//
//    @Override
//    public void setSessionId(HttpServletRequest request, HttpServletResponse response, String sessionId) {
//        if (!sessionId.equals(request.getAttribute(WRITTEN_SESSION_ID_ATTR))) {
//            request.setAttribute(WRITTEN_SESSION_ID_ATTR, sessionId);
//            this.cookieSerializer.writeCookieValue(new CookieSerializer.CookieValue(request, response, sessionId));
//        }
//    }
//
//    @Override
//    public void expireSession(HttpServletRequest request, HttpServletResponse response) {
//        this.cookieSerializer.writeCookieValue(new CookieSerializer.CookieValue(request, response, ""));
//    }
//
//    public void setCookieSerializer(CookieSerializer cookieSerializer) {
//        if (cookieSerializer == null) {
//            throw new IllegalArgumentException("cookieSerializer cannot be null");
//        } else {
//            this.cookieSerializer = cookieSerializer;
//        }
//    }
//}