package vip.wangwenhao.autoconfigure.aspect;//package vip.wangwenhao.common.aspect;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//import vip.wangwenhao.common.constants.CommonConstants;
//import vip.wangwenhao.common.enums.ResultCode;
//import vip.wangwenhao.common.exception.CommonException;
//import vip.wangwenhao.common.util.IpUtils;
//import vip.wangwenhao.common.util.PathMatcherUtil;
//
//import javax.servlet.http.HttpServletRequest;
//
///**
// * 控制所有请求调用都经过网关
// *
// * @author wwh
// * @date 2019年12月25日 10:11
// */
//@Aspect
//@Configuration
//@Slf4j
//public class GatewayAuthAspect {
//
//    @Around("execution(public * *(..)) && (@within(org.springframework.web.bind.annotation.RestController) || @within(org.springframework.stereotype.Controller))")
//    public Object auth(ProceedingJoinPoint joinPoint) throws Throwable {
//        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (null == attributes) {
//            return joinPoint.proceed();
//        }
//        HttpServletRequest request = attributes.getRequest();
//        String requestURI = request.getRequestURI();
//        if (!PathMatcherUtil.matches(CommonConstants.GATEWAY_AUTH_LINK_LIST,requestURI) || PathMatcherUtil.matches(CommonConstants.GATEWAY_ALLOW_LINK_LIST,requestURI)) {
//            return joinPoint.proceed();
//        }
//        String secretKey = request.getHeader("from");
//        if (StringUtils.isNotBlank(secretKey) && CommonConstants.FROM_SECRET.equals(secretKey)) {
//            return joinPoint.proceed();
//        } else {
//            log.error("{} invalid request from {}", request.getRequestURI(), IpUtils.getIpAddress());
//            throw new CommonException(ResultCode.UNAUTHORIZED_REQUEST);
//        }
//    }
//}