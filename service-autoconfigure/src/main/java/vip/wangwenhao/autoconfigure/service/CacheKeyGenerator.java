package vip.wangwenhao.autoconfigure.service;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author wangwenhao
 */
public interface CacheKeyGenerator {

    /**
     * 获取AOP参数,生成指定缓存Key
     *
     * @param pjp
     * @return
     */
    String getLockKey(ProceedingJoinPoint pjp);
}