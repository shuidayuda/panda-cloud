package vip.wangwenhao.autoconfigure.oauth2;

import org.springframework.context.annotation.Bean;

/**
 * @author wwh
 * @date 2020年01月19日 22:21
 */

public class Oauth2ClientCustomExceptionConfig {

    @Bean
    public CustomAccessDeniedHandler customAccessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Bean
    public CustomAuthenticationEntryPoint customAuthenticationEntryPoint() {
        return new CustomAuthenticationEntryPoint();
    }

}