package vip.wangwenhao.autoconfigure.oauth2;

import org.springframework.context.annotation.Bean;

/**
 * @author wwh
 * @date 2020年01月19日 22:26
 */

public class Oauth2ClientConfig{

    @Bean
    public AuthenticationTokenFilter authenticationTokenFilter() {
        return new AuthenticationTokenFilter();
    }

}