package vip.wangwenhao.autoconfigure.oauth2;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import vip.wangwenhao.common.enums.ResultCode;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.common.util.HttpUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wwh
 * @date 2019年12月27日 15:47
 */

@Slf4j
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        HttpUtils.response(response, HttpStatus.SC_FORBIDDEN, Result.fail(ResultCode.ACCESS_DENIED_EXCEPTION, e.getMessage()));
    }
}