package vip.wangwenhao.autoconfigure.utils;

import vip.wangwenhao.autoconfigure.constants.Oauth2Constants;

import java.util.Calendar;
import java.util.Date;

/**
 * @author wwh
 * @date 2019年12月29日 17:33
 */

public class DateUtils {

    public static Boolean isExpired(Date lastUpdatePassword) {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MONTH, Oauth2Constants.PASSWORD_EXPIRED_MONTH);
        Date time = now.getTime();
        return time.before(lastUpdatePassword);
    }

}