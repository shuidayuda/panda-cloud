package vip.wangwenhao.autoconfigure.constants;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

/**
 * @author wwh
 * @date 2020年01月15日 15:47
 */

public interface Oauth2Constants {

    String CLIENT_INFO_PREFIX = "Basic";

    Integer CLIENT_INFO_LENGTH = 2;

    String CLIENT_INFO_DELIMITER = ":";

    String[] OAUTH_ALLOW_LINK_LIST = {"/hystrix.stream", "/static/**", "/error/**", "/webjars/**", "/favicon.ico"};

    String BEARER_AUTHENTICATION = "Bearer ";

    List<String> AUTHORIZATION_SCOPE = Arrays.asList("insert", "delete", "update", "select");

    String ACCESS_TOKEN_URL = "/oauth2/token";

    String ACCESS_TOKEN_QUERY_NAME = "access_token";

    Integer PASSWORD_EXPIRED_MONTH = -3;

}