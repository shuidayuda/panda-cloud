package vip.wangwenhao.autoconfigure.constants;

import java.util.Arrays;
import java.util.List;

/**
*  
*
* @author wwh
* @date 2020年01月19日 17:34
* 
*/
    
public interface GatewayConstants {

    String GATEWAY_AUTH_HEADER = "from";

    String FROM_SECRET = "gateway";

    List<String> GATEWAY_AUTH_LINK_LIST = Arrays.asList("/api/**", "/user/**", "/open/**", "/item/**");

    List<String> GATEWAY_ALLOW_LINK_LIST = Arrays.asList("/**/login");

}