package vip.wangwenhao.autoconfigure.constants;

/**
*  
*
* @author wwh
* @date 2020年01月19日 17:33
* 
*/
    
public interface SessionConstants {

    String COOKIE_HEADER = "Cookie";

    String SESSION_HEADER = "SESSION";

    String SESSION_COOKIE_KEY = "SESSION";

    String TOKEN_SESSION_KEY = "access_token";

}