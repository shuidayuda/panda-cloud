package vip.wangwenhao.sso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("authorizationRequest")
public class OAuth2ApprovalController {

    @GetMapping("/oauth/confirm_access")
    public String getAccessConfirmation() {
        return "oauth_approval";
    }

}