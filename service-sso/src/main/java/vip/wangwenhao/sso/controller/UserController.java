package vip.wangwenhao.sso.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wwh
 * @date 2019年12月25日 14:14
 */

@RestController
@RequestMapping("user")
public class UserController {

    @PreAuthorize("hasAuthority('user')")
    @RequestMapping(value = "get", method = RequestMethod.GET)
    public UserDetails currentUserName(Authentication authentication) {
        return (UserDetails) authentication.getPrincipal();
    }

}