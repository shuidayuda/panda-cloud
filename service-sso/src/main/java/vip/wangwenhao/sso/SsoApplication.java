package vip.wangwenhao.sso;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.context.request.RequestContextListener;

/**
 * @author wangwenhao
 */
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "vip.wangwenhao.*.api.rpcservice")
@EnableRedisHttpSession
@SpringBootApplication
@ComponentScan(basePackages = "vip.wangwenhao")
@MapperScan("vip.wangwenhao.sso.mapper")
public class SsoApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SsoApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SsoApplication.class);
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }
}