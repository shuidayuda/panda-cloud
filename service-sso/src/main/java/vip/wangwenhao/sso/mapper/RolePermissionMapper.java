package vip.wangwenhao.sso.mapper;
import vip.wangwenhao.sso.entity.RolePermission;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface RolePermissionMapper {
    
    void insert(RolePermission rolePermission);

    int update(RolePermission rolePermission);

    List<RolePermission> query(RolePermission rolePermission);

    int delete(Long permissionId, Long roleId);

    RolePermission selectByPrimaryKey(Long permissionId, Long roleId);

}
