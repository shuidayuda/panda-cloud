package vip.wangwenhao.sso.mapper;
import vip.wangwenhao.sso.entity.Role;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface RoleMapper {
    
    void insert(Role role);

    int update(Role role);

    List<Role> query(Role role);

    int delete(Long id);

    Role selectByPrimaryKey(Long id);

}
