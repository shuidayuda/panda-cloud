package vip.wangwenhao.sso.mapper;
import vip.wangwenhao.sso.entity.User;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface UserMapper {
    
    void insert(User user);

    int update(User user);

    List<User> query(User user);

    int delete(Long id);

    User selectByPrimaryKey(Long id);

    User selectByUsername(String username);

}
