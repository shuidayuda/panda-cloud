package vip.wangwenhao.sso.mapper;
import vip.wangwenhao.sso.entity.Permission;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface PermissionMapper {
    
    void insert(Permission permission);

    int update(Permission permission);

    List<Permission> query(Permission permission);

    int delete(Long id);

    Permission selectByPrimaryKey(Long id);

}
