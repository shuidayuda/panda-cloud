package vip.wangwenhao.sso.mapper;
import vip.wangwenhao.sso.entity.UserRole;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface UserRoleMapper {
    
    void insert(UserRole userRole);

    int update(UserRole userRole);

    List<UserRole> query(UserRole userRole);

    int delete(Long roleId, Long userId);

    UserRole selectByPrimaryKey(Long roleId, Long userId);

}
