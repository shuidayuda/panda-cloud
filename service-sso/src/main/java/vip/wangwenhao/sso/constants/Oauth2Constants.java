package vip.wangwenhao.sso.constants;

import java.util.Arrays;
import java.util.List;

/**
 * @author wwh
 * @date 2019年12月29日 05:14
 */

public interface Oauth2Constants {

    String JWT_FILE_NAME = "panda-jwt.jks";

    String JWT_SECRET = "shuidayuda";

    String JWT_KEY_PAIR = "panda-jwt";

    String ROLE_PREFIX = "ROLE_";

    String TOKEN_LOGIN_URL = "/token/login";

    List<String> AUTH_CLIENT_URL = Arrays.asList("/oauth/token", "/oauth/check_token", "/token");

}