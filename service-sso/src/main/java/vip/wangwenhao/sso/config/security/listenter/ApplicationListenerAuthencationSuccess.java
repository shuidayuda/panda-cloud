package vip.wangwenhao.sso.config.security.listenter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationListenerAuthencationSuccess implements ApplicationListener<AuthenticationSuccessEvent> {
 
	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		log.info("{} login success",event.getAuthentication().getName());
	}
}