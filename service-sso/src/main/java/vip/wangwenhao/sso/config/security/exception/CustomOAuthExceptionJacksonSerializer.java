package vip.wangwenhao.sso.config.security.exception;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import vip.wangwenhao.common.enums.ResultCode;

import java.io.IOException;

public class CustomOAuthExceptionJacksonSerializer extends StdSerializer<CustomOAuth2Exception> {

    protected CustomOAuthExceptionJacksonSerializer() {
        super(CustomOAuth2Exception.class);
    }

    @Override
    public void serialize(CustomOAuth2Exception value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("code", ResultCode.AUTHORIZATION_INVALID);
        jsonGenerator.writeStringField("message", value.getMessage());
        jsonGenerator.writeBooleanField("isSuccess", false);
        jsonGenerator.writeEndObject();
    }
}