package vip.wangwenhao.sso.config.security.filter;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import vip.wangwenhao.sso.config.security.handler.CustomAuthenticationFailureHandler;
import vip.wangwenhao.sso.config.security.handler.CustomAuthenticationSuccessHandler;

/**
 * @author wwh
 * @date 2020年01月17日 01:15
 */
public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    public CustomUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager, CustomAuthenticationFailureHandler customAuthenticationFailureHandler, CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler) {
        super();
        setAuthenticationManager(authenticationManager);
        setAuthenticationFailureHandler(customAuthenticationFailureHandler);
        setAuthenticationSuccessHandler(customAuthenticationSuccessHandler);
    }

}