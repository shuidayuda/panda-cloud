package vip.wangwenhao.sso.config.session;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.session.data.redis.SessionOperation;

/**
*  
*
* @author wwh
* @date 2020年01月17日 23:27
* 
*/

@Configuration
public class SessionConfig {

    @Bean
    public SessionOperation sessionOperation(RedisOperationsSessionRepository redisOperationsSessionRepository){
        return new SessionOperation(redisOperationsSessionRepository);
    }

}