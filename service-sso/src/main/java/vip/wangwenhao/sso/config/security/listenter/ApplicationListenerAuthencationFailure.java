package vip.wangwenhao.sso.config.security.listenter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureServiceExceptionEvent;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationListenerAuthencationFailure implements ApplicationListener<AuthenticationFailureServiceExceptionEvent> {
 
	@Override
	public void onApplicationEvent(AuthenticationFailureServiceExceptionEvent authenticationFailureServiceExceptionEvent) {
		log.error("{}",authenticationFailureServiceExceptionEvent.getException());
	}
}