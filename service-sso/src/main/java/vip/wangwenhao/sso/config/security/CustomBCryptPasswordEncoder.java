package vip.wangwenhao.sso.config.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import vip.wangwenhao.common.util.EncryptUtils;

/**
 * @author wwh
 * @date 2019年12月26日 16:32
 */
public class CustomBCryptPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        return EncryptUtils.md5((String)charSequence);
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return s.equals(encode(charSequence));
    }
}