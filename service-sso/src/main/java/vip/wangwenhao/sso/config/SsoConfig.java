package vip.wangwenhao.sso.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import vip.wangwenhao.autoconfigure.annotations.EnableErrorPage;
import vip.wangwenhao.autoconfigure.annotations.EnableRedisConfig;
import vip.wangwenhao.autoconfigure.annotations.EnableSwaggerAndWebMvcConfig;
import vip.wangwenhao.autoconfigure.base.WebMvcConfig;
import vip.wangwenhao.sso.interceptors.InitInterceptor;

/**
 * @author wwh
 * @date 2020年01月19日 22:32
 */

@Configuration
@EnableRedisConfig
@EnableErrorPage
@EnableSwaggerAndWebMvcConfig
public class SsoConfig extends WebMvcConfig {

    @Autowired
    private InitInterceptor initInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //  基础拦截器
        registry.addInterceptor(initInterceptor)
                .addPathPatterns("/**");


        super.addInterceptors(registry);
    }
}