package vip.wangwenhao.sso.config.security.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.stereotype.Component;
import vip.wangwenhao.sso.config.security.exception.CustomWebResponseExceptionTranslator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wwh
 * @date 2019年12月27日 15:47
 */

@Component
public class CustomAccessDeniedHandler extends OAuth2AccessDeniedHandler {

    @Autowired
    private CustomWebResponseExceptionTranslator customWebResponseExceptionTranslator;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        this.setExceptionTranslator(customWebResponseExceptionTranslator);
        this.doHandle(request, response, e);
    }
}