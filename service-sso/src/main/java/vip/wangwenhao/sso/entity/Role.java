package vip.wangwenhao.sso.entity;


import lombok.*;

/**
 * p_role 实体类
 * @author wangwenhao
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Role{
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private String roleName;

}

