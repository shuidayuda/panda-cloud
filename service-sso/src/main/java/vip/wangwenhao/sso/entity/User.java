package vip.wangwenhao.sso.entity;


import lombok.*;

import java.util.Date;

/**
 * p_user 实体类
 *
 * @author wangwenhao
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class User {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String username;

    /**
     *
     */
    private String password;

    /**
     *
     */
    private Integer iteration;

    /**
     *
     */
    private String salt;

    /**
     *
     */
    private Date lastUpdatePassword;

    /**
     *
     */
    private Boolean locked;

    /**
     *
     */
    private Boolean disabled;

}

