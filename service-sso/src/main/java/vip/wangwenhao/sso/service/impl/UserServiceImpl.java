package vip.wangwenhao.sso.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.wangwenhao.common.util.BeanUtils;
import vip.wangwenhao.common.constants.CommonConstants;
import vip.wangwenhao.common.enums.ResultCode;
import vip.wangwenhao.common.exception.CommonException;
import vip.wangwenhao.sso.dto.UserDTO;
import vip.wangwenhao.sso.entity.User;
import vip.wangwenhao.sso.mapper.UserMapper;
import vip.wangwenhao.sso.service.UserService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wwh
 * @date 2019年12月29日 05:29
 */

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public UserDTO queryByUsername(String username) {
        try {
            List<User> users = userMapper.query(User.builder().username(username).build());
            if (!CollectionUtils.isEmpty(users)) {
                return BeanUtils.copyTo(users.get(CommonConstants.ZERO), UserDTO.class);
            }
        } catch (Exception e) {
            log.error("queryByUsername error", e);
            throw new CommonException(ResultCode.DB_OPR_FAIL, "queryByUsername error");
        }
        return null;
    }
}