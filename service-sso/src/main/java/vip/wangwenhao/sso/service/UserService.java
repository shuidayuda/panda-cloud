package vip.wangwenhao.sso.service;


import vip.wangwenhao.sso.dto.UserDTO;

/**
 * @author wwh
 * @date 2019年12月03日 15:06
 */

public interface UserService {

    /**
     * 根据用户名查询用户信息
     *
     * @param username
     * @return
     */
    UserDTO queryByUsername(String username);

}