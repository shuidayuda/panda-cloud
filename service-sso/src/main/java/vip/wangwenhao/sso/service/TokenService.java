package vip.wangwenhao.sso.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import vip.wangwenhao.common.result.Result;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wwh
 * @date 2020年01月15日 10:33
 */

public interface TokenService {

    /**
     * 根据token 保持登录状态
     * @param request
     * @return
     */
    Result<JSONObject> login(HttpServletRequest request);

    /**
     * 根据token 保持登录状态
     * @param request
     * @param authentication
     * @return
     */
    Result<OAuth2AccessToken> get(HttpServletRequest request, Authentication authentication);

    /**
     * 根据token
     * @param authentication
     * @return
     */
    Result<OAuth2AccessToken> get(Authentication authentication);

}