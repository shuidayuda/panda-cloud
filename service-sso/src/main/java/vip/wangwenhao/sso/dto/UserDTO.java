package vip.wangwenhao.sso.dto;

import lombok.*;

/**
 * @author wwh
 * @date 2019年12月30日 13:06
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class UserDTO {

    /**
     *
     */
    private String username;

    /**
     *
     */
    private String email;


}