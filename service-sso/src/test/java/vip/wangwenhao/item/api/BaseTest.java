package vip.wangwenhao.item.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.mq.api.dto.Message;
import vip.wangwenhao.mq.api.enums.MessageTypeEnum;
import vip.wangwenhao.mq.api.rpcservice.WebsocketRpcService;
import vip.wangwenhao.common.util.ServiceUtils;
import vip.wangwenhao.sso.SsoApplication;
import vip.wangwenhao.sso.service.TokenService;

import java.util.List;

/**
 * @author wwh
 * @date 2019年12月12日 15:21
 */

@SpringBootTest(classes = SsoApplication.class)
@RunWith(SpringRunner.class)
public class BaseTest {

    @Autowired
    private WebsocketRpcService websocketRpcService;

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private DiscoveryClient discoveryClient;
    @Autowired
    private TokenService tokenService;


    @Test
    public void test() {
        Message message = Message.builder()
                .uid("7b543db8a2194b56b20a92051ba6b5dd")
                .code(MessageTypeEnum.USER_OTHER_DEVICE_LOGIN.getCode())
                .content("有其他设备登录该账号，若是可信操作请忽略，否则请修改密码")
                .build();
//        websocketRpcService.sendToUser(message);
//        Result result = HttpUtils.post("http://localhost:8095/mq/ws/user", message, Result.class);
        Result<Message> result = ServiceUtils.service("service-mq", "/mq/ws/user", message, Message.class);
        System.out.println(JSONObject.toJSONString(result));
    }

    @Test
    public void testEureka() {
        Result<OAuth2AccessToken> oAuth2AccessTokenResult = tokenService.get(new UsernamePasswordAuthenticationToken("admin", "123456"));
        OAuth2AccessToken oAuth2AccessToken = oAuth2AccessTokenResult.getData();
        String accessToken = oAuth2AccessToken.getValue();
        System.out.println(accessToken);
        List<String> services = discoveryClient.getServices();
        System.out.println(services);
        services.stream().forEach(service -> {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            System.out.println(JSONArray.toJSONString(instances));

            String rsp = restTemplate.getForEntity("http://" + service + "/swagger-resources?access_token="+accessToken, String.class).getBody();

            //通过swagger查询服务接口的接口来获取该服务节点下的所有服务接口明细
//            HttpHeaders headers = new HttpHeaders();
//            headers.add(HttpHeaders.AUTHORIZATION, accessToken);
//            HttpEntity<?> entity = new HttpEntity<>(headers);
//            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://" + service + "/swagger-resources?access_token="+accessToken)
//                    .queryParam("access_token", accessToken);
//            HttpEntity<String> response = restTemplate.exchange(
//                    builder.build().encode().toUri(),
//                    HttpMethod.GET,
//                    entity,
//                    String.class);
//            String rsp = response.getBody();
            System.out.println(rsp);
            //使用fastjson进行解析
            JSONArray jsonArray = JSONArray.parseArray(rsp);
            jsonArray.stream().forEach(object->{
                JSONObject jsonObject = (JSONObject) object;
                String url = jsonObject.getString("url");
                String res = restTemplate.getForEntity("http://" + service + url + "&access_token="+accessToken, String.class).getBody();
                System.out.println(res);
            });
        });
    }
}