package vip.wangwenhao.sso.api.rpcservice;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.DefaultHystrixFallbackFactory;
import org.springframework.cloud.openfeign.FallbackEnableFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import vip.wangwenhao.common.result.Result;

/**
 * @author wwh
 * @date 2020年01月15日 11:03
 */

@FeignClient(name = "service-sso", fallbackFactory = DefaultHystrixFallbackFactory.class)
public interface TokenRpcService extends FallbackEnableFeignClient {

    /**
     * 根据token保持登录状态
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/token/login", method = RequestMethod.POST)
    Result<JSONObject> login(@RequestParam(value = "access_token", required = false) String accessToken);

    /**
     * accessToken 解密
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/token/read", method = RequestMethod.POST)
    Result<JSONObject> read(@RequestParam("access_token") String accessToken);
}