package vip.wangwenhao.mq.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.mq.api.dto.Message;
import vip.wangwenhao.common.result.Result;


@RestController
@RequestMapping("rabbitmq")
public class RabbitmqController {
 
    private final RabbitTemplate rabbitTemplate;

    public RabbitmqController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @GetMapping("/send")
    public Result<Message> sendDirectMessage(@RequestBody Message message) {
        rabbitTemplate.convertAndSend("TestDirectExchange", "TestDirectRouting", message);
        return Result.success(message);
    }
 
 
}