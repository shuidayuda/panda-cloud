package vip.wangwenhao.mq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.mq.service.RedisService;

/**
*  
*
* @author wwh
* @date 2020年01月19日 23:12
* 
*/

@RestController
public class RedisController {

    @Autowired
    private RedisService redisService;

    @Autowired
    private RedisTemplate<String, Object> objectRedisTemplate;

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }

    @RequestMapping("/public-message")
    public String publicMessage() {
        redisService.sendMessage("chat", "233444");
        return "success";
    }

    @RequestMapping("/pushQueue")
    public String pushToQueue() {
        objectRedisTemplate.opsForList().leftPush("KEY", System.currentTimeMillis());
        return "finish";
    }

}