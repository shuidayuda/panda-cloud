package vip.wangwenhao.mq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.mq.api.dto.Message;

/**
 * @author wwh
 * @date 2020年01月20日 19:12
 */

@RestController
@RequestMapping("/ws")
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate template;

    @RequestMapping(value = "/topic", method = RequestMethod.POST)
    public Result<Message> sendToTopic(@RequestBody Message message) {
        this.template.convertAndSend("/queue/getResponse", message);
        return Result.success(null);
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Result<Message> sendToUser(@RequestBody Message message) {
        String uid = message.getUid();
        this.template.convertAndSendToUser(uid, "/queue/getResponse", message);
        return Result.success(null);
    }
}