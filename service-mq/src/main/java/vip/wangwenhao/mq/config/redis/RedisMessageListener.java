package vip.wangwenhao.mq.config.redis;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class RedisMessageListener implements MessageListener {
 
    @Resource
    private RedisTemplate<String, Object> objectRedisTemplate;
 
 
    @Override
    public void onMessage(Message message, byte[] pattern) {
        byte[] body = message.getBody();
        byte[] channel = message.getChannel();
        // 请参考配置文件，本例中key，value的序列化方式均为string。
        // 其中key必须为stringSerializer。和redisTemplate.convertAndSend对应
        Object msgContent = objectRedisTemplate.getValueSerializer().deserialize(body);
        String topic = objectRedisTemplate.getStringSerializer().deserialize(channel);
        System.out.println("topic:" + topic + "msgContent:" + msgContent);
    }
}