package vip.wangwenhao.mq.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import vip.wangwenhao.mq.config.redis.RedisMessageListener;
import vip.wangwenhao.mq.interceptors.InitInterceptor;
import vip.wangwenhao.autoconfigure.annotations.EnableOauth2Client;
import vip.wangwenhao.autoconfigure.annotations.EnableRedisConfig;
import vip.wangwenhao.autoconfigure.base.WebMvcConfig;

/**
 * @author wwh
 * @date 2020年01月19日 22:10
 */

@Configuration
@EnableRedisConfig
@EnableOauth2Client
public class MqConfig extends WebMvcConfig {

    @Autowired
    private InitInterceptor initInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //  基础拦截器
        registry.addInterceptor(initInterceptor)
                .addPathPatterns("/**");


        super.addInterceptors(registry);
    }

    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, new PatternTopic("chat"));

        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(RedisMessageListener receiver) {
        return new MessageListenerAdapter(receiver);
    }

}