package vip.wangwenhao.mq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RedisQueueConsumer {

    @Autowired
    private RedisTemplate<String, Object> objectRedisTemplate;


    @PostConstruct
    public void init() {
        Thread thread = new Thread(() -> {
            while (true) {
                Object key = objectRedisTemplate.opsForList().leftPop("KEY");
                if (key != null) {
                    System.out.println(key.toString());
                }
            }
        });
        thread.start();
    }
}