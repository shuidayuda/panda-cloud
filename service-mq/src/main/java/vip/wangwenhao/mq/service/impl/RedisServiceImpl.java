package vip.wangwenhao.mq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import vip.wangwenhao.mq.service.RedisService;

@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void sendMessage(String channel, String message) {
        redisTemplate.convertAndSend(channel, message);
    }
}