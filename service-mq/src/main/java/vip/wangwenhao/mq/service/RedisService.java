package vip.wangwenhao.mq.service;

/**
*  
*
* @author wwh
* @date 2020年01月20日 00:19
* 
*/
    
public interface RedisService {

    /**
     * redis发布消息
     *
     * @param channel
     * @param message
     */
    void sendMessage(String channel, String message);
}