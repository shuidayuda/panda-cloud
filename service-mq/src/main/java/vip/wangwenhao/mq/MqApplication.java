package vip.wangwenhao.mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.context.request.RequestContextListener;
import vip.wangwenhao.common.constants.CommonConstants;
import vip.wangwenhao.mq.service.ShopChannel;

/**
*  
*
* @author wwh
* @date 2020年01月19日 18:41
* 
*/

@EnableDiscoveryClient
@EnableFeignClients(basePackages = "vip.wangwenhao.*.api.rpcservice")
@SpringBootApplication(scanBasePackages = {CommonConstants.BASE_PACKAGE}, exclude = {DataSourceAutoConfiguration.class})
@EnableRedisHttpSession
@EnableBinding(ShopChannel.class)
@EnableScheduling
public class MqApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(MqApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MqApplication.class);
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

}