package vip.wangwenhao.item.entity;


/**
 * p_product 实体类
 * @author wangwenhao
 */ 
public class Product{
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private String productName;

	/**
	 *  
	 */
	private Long productPrice;

	/**
	 *  
	 */
	private String productDesc;

	/**
	 *  
	 */
	private String productImg;

	/**
	 *  
	 */
	private Integer productStock;

	/**
	 *  
	 */
	private Integer productLock;

	public void setId(Long id){
		this.id = id;
	}

	public Long getId(){
		return this.id;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return this.productName;
	}

	public void setProductPrice(Long productPrice){
		this.productPrice = productPrice;
	}

	public Long getProductPrice(){
		return this.productPrice;
	}

	public void setProductDesc(String productDesc){
		this.productDesc = productDesc;
	}

	public String getProductDesc(){
		return this.productDesc;
	}

	public void setProductImg(String productImg){
		this.productImg = productImg;
	}

	public String getProductImg(){
		return this.productImg;
	}

	public void setProductStock(Integer productStock){
		this.productStock = productStock;
	}

	public Integer getProductStock(){
		return this.productStock;
	}

	public void setProductLock(Integer productLock){
		this.productLock = productLock;
	}

	public Integer getProductLock(){
		return this.productLock;
	}

}

