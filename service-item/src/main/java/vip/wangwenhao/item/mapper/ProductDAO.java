package vip.wangwenhao.item.mapper;
import org.apache.ibatis.annotations.Param;
import vip.wangwenhao.item.entity.Product;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface ProductDAO {
    
    int insert(Product product);

    int update(Product product);

    int decrStock(@Param("id") Long id, @Param("num")Integer num);

    int lockStock(@Param("id") Long id, @Param("num")Integer num);

    int releaseStock(@Param("id") Long id, @Param("num")Integer num);

    List<Product> query(Product product);

    int delete(Long id);

    Product selectByPrimaryKey(Long id);

}
