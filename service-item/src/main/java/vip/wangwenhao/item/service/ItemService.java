package vip.wangwenhao.item.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.item.api.dto.ProductDTO;

import java.util.List;

/**
 * @author wwh
 * @date 2019年12月12日 18:05
 */
@LocalTCC
public interface ItemService {

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Result<ProductDTO> queryById(Long id);

    @TwoPhaseBusinessAction(name = "decrStock",commitMethod = "confirmDecrStock",rollbackMethod = "cancelDecrStock")
    Result<Integer> decrStock(BusinessActionContext businessActionContext,@BusinessActionContextParameter(paramName = "id")Long id, @BusinessActionContextParameter(paramName = "num")Integer num);
    Boolean confirmDecrStock(BusinessActionContext businessActionContext);
    Boolean cancelDecrStock(BusinessActionContext businessActionContext);

    Result<Integer> insert(ProductDTO productDTO);

    Result<Integer> update(ProductDTO productDTO);

    Result<List<ProductDTO>> query(ProductDTO productDTO);

    Result<Integer>  delete(Long id);

}