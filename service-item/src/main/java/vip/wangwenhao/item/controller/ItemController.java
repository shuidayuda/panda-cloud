package vip.wangwenhao.item.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.item.api.dto.ProductDTO;
import vip.wangwenhao.item.service.ItemService;

import java.util.List;

/**
 * @author wwh
 * @date 2019年12月12日 18:11
 */
@RestController
@Slf4j
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "queryById", method = RequestMethod.GET)
    public Result<ProductDTO> queryById(@RequestParam("id") Long id) {
        return itemService.queryById(id);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public Result<Integer> update(@RequestBody ProductDTO productDTO) {
        return itemService.update(productDTO);
    }

    @RequestMapping(value = "decrStock", method = RequestMethod.POST)
    public Result<Integer> decrStock(@RequestParam("id") Long id, @RequestParam("num") Integer num) {
        log.warn("itemController decrStock {} {}", id, num);
        return itemService.decrStock(null,id, num);
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    public Result<Integer> insert(@RequestBody ProductDTO productDTO) {
        return itemService.insert(productDTO);
    }

    @RequestMapping(value = "insert", method = RequestMethod.GET)
    public Result<Integer> delete(@RequestParam("id") Long id) {
        return itemService.delete(id);
    }

    @RequestMapping(value = "query", method = RequestMethod.POST)
    public Result<List<ProductDTO>> query(@RequestBody ProductDTO productDTO) {
        return itemService.query(productDTO);
    }

}