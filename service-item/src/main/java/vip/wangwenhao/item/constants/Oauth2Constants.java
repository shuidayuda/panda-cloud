package vip.wangwenhao.item.constants;

/**
 * @author wwh
 * @date 2020年01月15日 17:04
 */

public interface Oauth2Constants {
    String TOKEN_LOGIN_URL = "/item/token/login";
}