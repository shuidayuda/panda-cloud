package vip.wangwenhao.item.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vip.wangwenhao.item.ItemApplication;

/**
 * @author wwh
 * @date 2019年12月12日 15:21
 */

@SpringBootTest(classes = ItemApplication.class)
@RunWith(SpringRunner.class)
public class BaseTest {

    @Test
    public void test() {
        System.out.println("ok");
    }
}