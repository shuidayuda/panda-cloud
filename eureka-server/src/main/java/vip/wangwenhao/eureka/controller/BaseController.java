package vip.wangwenhao.eureka.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.eureka.config.BaseInfo;

/**
 * @author wwh
 * @date 2019年12月12日 11:26
 */

@RestController
public class BaseController {

    @Autowired
    private BaseInfo baseInfo;

    @RequestMapping("baseInfo")
    public BaseInfo baseInfo() {
        BaseInfo baseInfo = new BaseInfo();
        BeanUtils.copyProperties(this.baseInfo, baseInfo);
        return baseInfo;
    }

}