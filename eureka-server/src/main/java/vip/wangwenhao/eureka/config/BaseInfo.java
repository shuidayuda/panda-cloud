package vip.wangwenhao.eureka.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @author wwh
 * @date 2019年12月12日 11:22
 */
@RefreshScope
@Configuration
@Data
public class BaseInfo {

    @Value("${panda.appKey}")
    private String appKey;

    @Value("${panda.secret}")
    private String secret;

    @Value("${panda.version}")
    private String version;


}