package vip.wangwenhao.common.constants;

/**
 * @author wwh
 * @date 2019年12月19日 16:53
 */

public interface SymbolConstants {

    String COLON = ":";

    String EQUAL = "=";

    String SEMICOLON = ";";

    String EMPTY = "";

    String BLANK = " ";


}