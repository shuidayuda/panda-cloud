package vip.wangwenhao.common.constants;

import java.nio.charset.Charset;

/**
 * @author wwh
 * @date 2019年12月19日 16:56
 */

public interface CommonConstants {

    Integer ZERO = 0;

    Integer ONE = 1;

    Integer TWO = 2;

    Integer FIVE = 5;

    Integer ELEVEN = 11;

    String BASE_PACKAGE = "vip.wangwenhao";

    String UNKNOWN = "unknown";

    Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    String UID_COOKIE_KEY = "uid";
}