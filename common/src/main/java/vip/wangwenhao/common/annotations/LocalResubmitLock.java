package vip.wangwenhao.common.annotations;


import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 本地防重
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Lock
public @interface LocalResubmitLock {

    /**
     * key
     *
     * @return String
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "key"
    )
    String value() default "";

    /**
     * 键
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "key"
    )
    String key() default "";

    /**
     * 分隔符
     *
     * @return
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "delimiter"
    )
    String delimiter() default ":";
}