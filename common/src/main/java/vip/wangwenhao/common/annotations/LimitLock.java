package vip.wangwenhao.common.annotations;

import org.springframework.core.annotation.AliasFor;
import vip.wangwenhao.common.constants.SymbolConstants;
import vip.wangwenhao.common.enums.LimitType;

import java.lang.annotation.*;

/**
 * 限流
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Lock
public @interface LimitLock {

    /**
     * key
     *
     * @return String
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "key"
    )
    String value() default "";

    /**
     * 资源的key
     *
     * @return String
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "key"
    )
    String key() default "";

    /**
     * Key的prefix
     *
     * @return String
     */
    String prefix() default "";

    /**
     * 给定的时间段
     * 单位秒
     *
     * @return int
     */
    int period();

    /**
     * 最多的访问限制次数
     *
     * @return int
     */
    int count();

    /**
     * 类型
     *
     * @return LimitType
     */
    LimitType limitType() default LimitType.CUSTOMER;

    /**
     * 分隔符
     *
     * @return
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "delimiter"
    )
    String delimiter() default SymbolConstants.COLON;

}