package vip.wangwenhao.common.annotations;


import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * lock 注解参数及别名
 */
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface LockParam {

    /**
     * 字段名称
     *
     * @return String
     */
    @AliasFor("value")
    String name() default "";

    /**
     * 字段名称
     *
     * @return String
     */
    @AliasFor("name")
    String value() default "";
}