package vip.wangwenhao.common.annotations;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 集群防重
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Lock
public @interface GroupResubmitLock {

    /**
     * key
     *
     * @return String
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "key"
    )
    String value() default "";

    /**
     * key
     *
     * @return
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "key"
    )
    String key() default "";

    /**
     * redis 锁key的前缀
     *
     * @return redis 锁key的前缀
     */
    String prefix() default "";

    /**
     * 过期秒数,默认为5秒
     *
     * @return 轮询锁的时间
     */
    long expire() default 5;

    /**
     * 超时时间单位
     *
     * @return 秒
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 分隔符
     *
     * @return
     */
    @AliasFor(
            annotation = Lock.class,
            attribute = "delimiter"
    )
    String delimiter() default ":";
}