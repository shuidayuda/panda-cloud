package vip.wangwenhao.common.annotations;


import java.lang.annotation.*;

/**
 * 所有Lock注解的共有属性
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Lock {

    /**
     * 键
     */
    String key() default "";

    /**
     * 分隔符
     *
     * @return
     */
    String delimiter() default ":";
}