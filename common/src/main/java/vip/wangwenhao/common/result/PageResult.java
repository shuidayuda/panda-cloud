package vip.wangwenhao.common.result;

import lombok.Data;
import vip.wangwenhao.common.enums.ResultCode;

import java.util.List;

/**
 * 接口返回数据结构规范
 *
 * @param <T>
 * @author wangwenhao
 */
@Data
public class PageResult<T> {

    private String code;
    private String msg;
    private Boolean isSuccess;
    private List<T> data;
    private Long total;

    public static <T> PageResult<T> success(List<T> data, Long total) {
        PageResult<T> result = new PageResult<>();
        result.setIsSuccess(Boolean.TRUE);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMessage());
        result.setData(data);
        result.setTotal(total);
        return result;
    }

    public static <T> PageResult<T> fail(ResultCode resultCode) {
        PageResult<T> result = new PageResult<>();
        result.setIsSuccess(Boolean.FALSE);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMessage());
        result.setData(null);
        return result;
    }

    public static <T> PageResult<T> fail(ResultCode resultCode, String msg) {
        PageResult<T> result = new PageResult<>();
        result.setIsSuccess(Boolean.FALSE);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMessage() + " : " + msg);
        result.setData(null);
        return result;
    }
}