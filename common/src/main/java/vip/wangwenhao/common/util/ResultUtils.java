package vip.wangwenhao.common.util;

import vip.wangwenhao.common.enums.ResultCode;
import vip.wangwenhao.common.result.PageResult;
import vip.wangwenhao.common.result.Result;

/**
 * @author wwh
 * @date 2019年12月20日 14:19
 */

public class ResultUtils {

    public static Object result(Class clazz) {
        return result(clazz, ResultCode.ERROR_SERVER_FAILED);
    }

    public static Object result(Class clazz, ResultCode resultCode) {
        return result(clazz, resultCode, resultCode.getMessage());
    }

    public static Object result(Class clazz, ResultCode resultCode, String message) {
        if (PageResult.class.equals(clazz)) {
            return PageResult.fail(resultCode, message);
        } else {
            return Result.fail(resultCode, message);
        }
    }
}