package vip.wangwenhao.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wwh
 * @date 2019年12月29日 17:33
 */

public class DateUtils {

    private final static SimpleDateFormat Y_M_D = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public static String format(Date date) {
        if (null == date) {
            return null;
        }
        try {
            return Y_M_D.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String format(Long date) {
        if (null == date) {
            return null;
        }
        try {
            return format(new Date(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}