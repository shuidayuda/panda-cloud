package vip.wangwenhao.common.util;

import vip.wangwenhao.common.exception.CommonException;
import vip.wangwenhao.common.exception.LimitException;

/**
 * @author wwh
 * @date 2019年12月25日 10:41
 */

public class ExceptionUtils {

    public static CommonException exception(Throwable throwable) {
        if (throwable instanceof LimitException) {
            return new LimitException();
        }
        return new CommonException(throwable.getMessage());
    }

}