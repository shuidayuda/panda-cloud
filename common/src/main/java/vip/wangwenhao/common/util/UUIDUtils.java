package vip.wangwenhao.common.util;

import java.util.UUID;

/**
*  
*
* @author wwh
* @date 2020年01月20日 22:34
* 
*/
    
public class UUIDUtils {
    /**
     * 获取UUID
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 获取UUID 去除—
     *
     * @return
     */
    public static String getEMUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}