package vip.wangwenhao.common.util;

import org.apache.commons.lang3.StringUtils;
import vip.wangwenhao.common.enums.ResultCode;
import vip.wangwenhao.common.exception.CommonException;

/**
 * @author wangwenhao
 */
public class CheckUtil {

    /**
     * 校验字符串是否为空
     *
     * @param str
     * @param resultCode
     * @param message
     * @return
     */
    public static String checkStringNotNull(String str, ResultCode resultCode, String message) {
        if (StringUtils.isEmpty(str) || StringUtils.isEmpty(str.trim())) {
            throw new CommonException(resultCode, message);
        }
        return str;
    }

    public static <T> T checkNotNull(T object, ResultCode resultCode, String message) {
        if (object == null) {
            throw new CommonException(resultCode, message);
        }
        return object;
    }

    public static void checkState(boolean expression, ResultCode resultCode, String detailMsg) {
        if (!expression) {
            throw new CommonException(resultCode, detailMsg);
        }
    }
}
