package vip.wangwenhao.common.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.cglib.beans.BeanCopier;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author wwh
 * @date 2019年12月12日 17:27
 */

public class BeanUtils {

    public static <T, S> List<T> copyTo(List<S> sourceList, Class<T> targetClazz) {
        checkNotNull(sourceList);
        List<T> result = new ArrayList<>();
        for (S s : sourceList) {
            result.add(copyTo(s, targetClazz));
        }
        return result;
    }

    public static <T, S> T copyTo(S source, Class<T> targetClazz) {
        checkNotNull(source);
        BeanCopier beanCopier = BeanCopier.create(source.getClass(), targetClazz, false);
        T target = newObject(targetClazz);
        beanCopier.copy(source, target, null);
        return target;
    }

    private static <T> T newObject(Class<T> targetClazz) {
        try {
            Constructor<T> constructor = targetClazz.getConstructor(new Class[0]);
            return (T) constructor.newInstance(new Object[0]);
        } catch (Exception e) {
            throw new RuntimeException("no constructor without any parameter" + targetClazz);
        }
    }

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();
        Set<String> emptyNames = new HashSet<>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    public static void copyProperties(Object source, Object target, boolean isIgnoreNullValue) {
        if (isIgnoreNullValue) {
            org.springframework.beans.BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
        } else {
            org.springframework.beans.BeanUtils.copyProperties(source, target);
        }
    }

    public static Map<String, Object> beanToMap(Object bean) {
        Map<String, Object> map = JSONObject.parseObject(JSONObject.toJSONString(bean, SerializerFeature.IgnoreNonFieldGetter), Map.class);
        return map;
    }

    public static Object mapToObject(Map<String, Object> map, Class<?> beanClass)  {
        if (map == null){
            return null;
        }


        try {
            Object obj = beanClass.newInstance();

            org.apache.commons.beanutils.BeanUtils.populate(obj, map);

            return obj;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Map<?, ?> objectToMap(Object obj) {
        if(obj == null){
            return null;
        }

        return new org.apache.commons.beanutils.BeanMap(obj);
    }

}