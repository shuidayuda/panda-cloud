package vip.wangwenhao.common.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wangwenhao
 */
public class WebUtils {

    /**
     * 获取用户请求真实IP
     *
     * @return
     */
    public static String getRemoteIp() {

        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        if (null != requestAttributes) {
            ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) requestAttributes);
            if (null != servletRequestAttributes) {
                HttpServletRequest request = servletRequestAttributes.getRequest();
                if (null != request) {
                    String ip = null;
                    //X-Forwarded-For：Squid 服务代理
                    String ipAddresses = request.getHeader("X-Forwarded-For");

                    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
                        //Proxy-Client-IP：apache 服务代理
                        ipAddresses = request.getHeader("Proxy-Client-IP");
                    }

                    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
                        //WL-Proxy-Client-IP：weblogic 服务代理
                        ipAddresses = request.getHeader("WL-Proxy-Client-IP");
                    }

                    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
                        //HTTP_CLIENT_IP：有些代理服务器
                        ipAddresses = request.getHeader("HTTP_CLIENT_IP");
                    }

                    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
                        //X-Real-IP：nginx服务代理
                        ipAddresses = request.getHeader("X-Real-IP");
                    }

                    //有些网络通过多层代理，那么获取到的ip就会有多个，一般都是通过逗号（,）分割开来，并且第一个ip为客户端的真实IP
                    if (ipAddresses != null && ipAddresses.length() != 0) {
                        ip = ipAddresses.split(",")[0];
                    }

                    //还是不能获取到，最后再通过request.getRemoteAddr();获取
                    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
                        ip = request.getRemoteAddr();
                    }
                    return ip;
                }
            }
        }
        return null;
    }

    /**
     * 获取当前访问的url
     *
     * @param request
     * @return
     */
    public static String getReqUrl(HttpServletRequest request) {
        return request.getHeader("Referer");
    }
}
