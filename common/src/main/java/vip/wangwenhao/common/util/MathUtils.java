package vip.wangwenhao.common.util;

import java.math.BigDecimal;

/**
 * @author wangwenhao
 */
public class MathUtils {

    /**
     * 由于Java的简单类型不能够精确的对浮点数进行运算，这个工具类提供精确的浮点数运算，包括加減乘除和四捨五入。
     */


    private static final int DEF_DIV_SCALE = 10;

    /**
     * BigDecimal 100
     */
    public static final BigDecimal DECIMAL_HUNDRED = new BigDecimal("100");

    private MathUtils() {

    }

    /**
     * 提供精确的加法运算
     *
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */
    public static double add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }

    /**
     * 提供精确的减法运算
     *
     * @param v1 被減数
     * @param v2 減数
     * @return两个参数的差
     */
    public static double sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }

    /**
     * 提供精确的乘法运算
     *
     * @param v1 被乘数
     * @param v2 乘数
     * @return 两个参数的积
     */
    public static double mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2).doubleValue();
    }

    /**
     * 提供（相对）精确的除非运算，当发生除不尽的情况时，精确到小数点以后10位，以后的数字四舍五入
     *
     * @param v1 被除數
     * @param v2 除數
     * @return 兩個參數的商
     */
    public static double div(double v1, double v2) {
        return div(v1, v2, DEF_DIV_SCALE);
    }

    /**
     * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指定精度，以后的数字四舍五入
     *
     * @param v1    被除數
     * @param v2    除數
     * @param scale 表示表示需要精確到小數點以後位数。
     * @return 兩個參數的商
     */
    public static double div(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 提供精確的小數位四捨五入處理。
     * 提供精确的小数位四舍五入处理
     *
     * @param v     需要四捨五入的數位
     * @param scale 小數點後保留幾位
     * @return 四捨五入後的結果
     */
    public static double round(double v, int scale) {
        if (v == 0) {
            return 0D;
        }
        BigDecimal b = new BigDecimal(Double.toString(v));
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 保留两位小数向下取整
     *
     * @param number
     * @return
     */
    public static BigDecimal roundDown2(BigDecimal number) {
        return number.setScale(2, BigDecimal.ROUND_DOWN);
    }


    /**
     * 向上取整，转为int
     *
     * @param amount
     * @return
     */
    public static int decimalToInt(BigDecimal amount) {
        return amount.setScale(0, BigDecimal.ROUND_UP).intValue();
    }



    /**
     * 两数相乘保留两位小数
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal multiplyWithScale2(BigDecimal num1, BigDecimal num2) {
        return num1.multiply(num2).setScale(2, BigDecimal.ROUND_DOWN);
    }

    /**
     * 两数相乘保留两位小数(四舍五入)
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal multiplyHalfUpWithScale2(BigDecimal num1, BigDecimal num2) {
        return num1.multiply(num2).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 两数相除保留两位小数
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal divideWithScale2(BigDecimal num1, BigDecimal num2) {
        return num1.divide(num2, 2, BigDecimal.ROUND_HALF_UP);
    }

}
