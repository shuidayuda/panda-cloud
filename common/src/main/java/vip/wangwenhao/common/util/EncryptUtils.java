package vip.wangwenhao.common.util;


import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @author wangwenhao
 */
public abstract class EncryptUtils {

    private static final Base64.Decoder decoder = Base64.getDecoder();
    private static final Base64.Encoder encoder = Base64.getEncoder();

    /**
     * 编码格式；默认使用uft-8
     */
    private static final String charset = "utf-8";

    private EncryptUtils() {
    }


    /**
     * 使用Base64进行加密
     *
     * @param res
     * @return
     */
    private static String base64(byte[] res) {
        return encoder.encodeToString(res);
    }

    /**
     * 使用Base64进行加密
     *
     * @param res
     * @return
     */
    public static String base64Encode(String res) {
        try {
            return base64(res.getBytes(charset));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 使用Base64进行解密
     *
     * @param res
     * @return
     */
    public static String base64Decode(String res) {
        try {
            return new String(decoder.decode(res), charset);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * MD5加密
     *
     * @param data 待加密数据
     * @return byte[] 消息摘要
     * @throws Exception
     */
    public static byte[] md5Byte(String data) throws Exception {

        // 执行消息摘要
        return DigestUtils.md5(data);
    }

    /**
     * MD5加密
     *
     * @param data 待加密数据
     * @return byte[] 消息摘要
     * @throws Exception
     */
    public static String md5(String data) {
        // 执行消息摘要
        return DigestUtils.md5Hex(data);
    }
}