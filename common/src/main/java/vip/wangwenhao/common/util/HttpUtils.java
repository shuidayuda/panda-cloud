package vip.wangwenhao.common.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpHeaders;
import vip.wangwenhao.common.result.Result;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wwh
 * @date 2019年12月29日 01:51
 */
@Slf4j
public class HttpUtils {

    public static <T> void response(HttpServletResponse response, int status, Result<T> t) {
        try {
            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(status);
            response.getWriter().write(JSONObject.toJSONString(t));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> void response(HttpServletResponse response, HttpHeaders headers, int status, Result<T> t) {
        if (null != headers && !headers.isEmpty()) {
            headers.toSingleValueMap().forEach((k, v) -> {
                response.setHeader(k, v);
            });
        }
        response(response, status, t);
    }

    public static void response(HttpServletResponse response, Object obj) {
        try {
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(JSONObject.toJSONString(obj));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T, F> Result<T> post(String url, F f, Class<T> t) {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/json; charset=UTF-8");
        httpPost.setHeader("accept", "*/*");
        httpPost.setHeader("accept-encoding", "gzip, deflate");
        StringEntity stringEntity = new StringEntity(JSONObject.toJSONString(f), ContentType.APPLICATION_JSON);
        httpPost.setEntity(stringEntity);
        Result<T> result = null;
        CloseableHttpClient client = HttpClients.custom().build();
        try {
            HttpResponse httpResponse = client.execute(httpPost);
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                result = JSONObject.parseObject(EntityUtils.toString(entity), new TypeReference<Result<T>>() {
                });
                return result;
            }
        } catch (Exception e) {
            log.error("HttpUtil|request|error|url={}|e={}", url, e);
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                log.info("HttpUtil|request|close|error={}", e);
            }
        }
        return null;
    }

}