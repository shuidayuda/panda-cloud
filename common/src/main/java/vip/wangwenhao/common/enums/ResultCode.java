package vip.wangwenhao.common.enums;

import com.alibaba.fastjson.JSONObject;

/**
 * @author wangwenhao
 */
public enum ResultCode {

    /**
     * 业务请求成功
     */
    SUCCESS("SUCCESS", "Business Success"),
    RUNTIME_EXCEPTION("FAILED", "业务处理异常"),
    UNAUTHORIZED_REQUEST("UNAUTHORIZED_REQUEST", "请求未授权"),
    AUTHORIZATION_INVALID("AUTHORIZATION_INVALID", "无效授权"),
    TIMEOUT_EXCEPTION("TIMEOUT_EXCEPTION", "接口超时"),
    DB_OPR_FAIL("DB_OPR_FAIL", "数据库操作异常"),
    SESSION_KEY_EXPIRED("SESSION_KEY_EXPIRED", "session过期"),
    INVALID_TOKEN("INVALID_TOKEN", "无效token"),
    ACCESS_DENIED_EXCEPTION("ACCESS_DENIED_EXCEPTION", "无权限访问"),
    ERROR_SERVER_FAILED("ERROR_SERVER_FAILED", "内部服务器错误"),
    SERVICE_NOT_FOUND("SERVICE_NOT_FOUND", "服务未发现"),
    RPC_SERVICE_EXEC_ERROR("RPC_SERVICE_EXEC_ERROR", "RPC调用失败"),
    ROUTE_ERROR("ROUTE_ERROR", "路由异常"),
    REQUEST_PARAM_CHECK_FAILED("REQUEST_PARAM_CHECK_FAILED", "请求参数校验失败"),
    TOO_MANY_REQUESTS("TOO_MANY_REQUESTS", "请求次数太多，请稍后重试"),
    RESUBMIT("RESUBMIT", "重复提交"),
    HYSTRIX_FALLBACK_EXCEPTION("HYSTRIX_FALLBACK_EXCEPTION", "断路器回调异常"),
    REQUEST_INVALID("REQUEST_INVALID", "请求无效"),
    REQUEST_PARAM_MISS("REQUEST_PARAM_MISS", "请求缺失"),
    REQUEST_ERROR("REQUEST_ERROR", "请求错误"),
    REQUEST_METHOD_NOT_SUPPORT("REQUEST_METHOD_NOT_SUPPORT", "请求方法不支持"),
    REQUEST_CONTENT_UNREAD("REQUEST_CONTENT_UNREAD", "请求内容不可读"),
    METHOD_PARAM_NOT_MATCH("METHOD_PARAM_NOT_MATCH", "方法参数类型不匹配"),
    METHOD_PARAM_CHECK_FAILED("REQUEST_PARAM_CHECK_FAILED", "方法参数验证失败"),
    RESPONSE_ERROR("RESPONSE_ERROR", "请求内容响应错误"),
    RESPONSE_CONTENT_NOT_SUPPORT("RESPONSE_CONTENT_NOT_SUPPORT", "响应的内容类型不支持"),
    UN_LOGIN("UN_LOGIN", "未登录"),
    LOGIN_ERROR("LOGIN_ERROR", "登录失败"),

    CREATE_ORDER_ERROR("CREATE_ORDER_ERROR", "下单失败"),

    ;

    private String code;
    private String message;

    ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ResultCode getResultCode(String code) {
        for (ResultCode resultCode : values()) {
            if (resultCode.getCode().equals(code)) {
                return resultCode;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JSONObject resultToJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", getCode());
        jsonObject.put("message", getMessage());
        return jsonObject;
    }

}