package vip.wangwenhao.common.enums;

public enum RedisKeysEnum {

    /**
     * 所有的key以PANDA开头
     */
    GLOBAL_PRESENT_ORDER_TIME("panda:user:", "用户信息前缀"),
    ;


    private String key;

    private String desc;

    RedisKeysEnum(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}