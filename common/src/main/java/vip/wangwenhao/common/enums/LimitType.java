package vip.wangwenhao.common.enums;

/**
 * @author wangwenhao
 */
public enum LimitType {
    /**
     * 自定义key
     */
    CUSTOMER,
    /**
     * 根据请求者IP
     */
    IP,
    /**
     * 根据请求者IP
     */
    METHOD,
    ;
}