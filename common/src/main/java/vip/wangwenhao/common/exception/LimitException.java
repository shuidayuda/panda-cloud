package vip.wangwenhao.common.exception;

import vip.wangwenhao.common.enums.ResultCode;

/**
 * @author wwh
 * @date 2019年12月17日 22:30
 */

public class LimitException extends CommonException {

    public LimitException() {
        super(ResultCode.TOO_MANY_REQUESTS);
    }

    public LimitException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LimitException{");
        sb.append("resultCode=").append(getResultCode().getCode());
        sb.append(",resultMsg=").append(getResultCode().getMessage());
        sb.append(", message=").append(getMessage());
        sb.append("}");
        return sb.toString();
    }
}