package vip.wangwenhao.common.exception;

import vip.wangwenhao.common.enums.ResultCode;

/**
 * @author wangwenhao
 */
public class CommonException extends RuntimeException {

    private String message;
    private ResultCode resultCode;

    public CommonException() {
        this.resultCode = ResultCode.ERROR_SERVER_FAILED;
    }

    public CommonException(ResultCode resultCode) {
        message = resultCode.getMessage();
        this.resultCode = resultCode;
    }

    public CommonException(String message) {
        this(ResultCode.ERROR_SERVER_FAILED,message);
    }

    public CommonException(ResultCode resultCode, String message) {
        this.resultCode = resultCode;
        this.message = message;

    }

    public ResultCode getResultCode() {
        return resultCode;
    }


    @Override
    public String getMessage() {
        return message;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CommonException{");
        sb.append("resultCode=").append(resultCode.getCode());
        sb.append(",resultMsg=").append(resultCode.getMessage());
        sb.append(", message=").append(message);
        sb.append("}");
        return sb.toString();
    }
}