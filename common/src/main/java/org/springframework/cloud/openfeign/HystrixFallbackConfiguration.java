package org.springframework.cloud.openfeign;

import feign.Feign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 开启统一的服务熔断|降级配置，
 * 通过动态代理实现统一的fallback逻辑，
 * 在@FeignClient注解中使用fallbackFactory=DefaultHystrixFallbackFactory.class，
 * 并且被@FeignClient注解的服务接口需要继承{@link FallbackEnableFeignClient}接口
 *
 * @author wangwenhao
 * @date 2019年6月1日 下午6:17:50
 */
@Configuration
@ConditionalOnClass(Feign.class)
public class HystrixFallbackConfiguration {


    @Configuration
    @ConditionalOnClass(name = "feign.hystrix.HystrixFeign")
    protected static class HystrixFeignTargeterConfiguration {
        @Bean
        @ConditionalOnMissingBean
        public Targeter feignTargeter() {
            return new HystrixFallbackTargeter();
        }
    }

}