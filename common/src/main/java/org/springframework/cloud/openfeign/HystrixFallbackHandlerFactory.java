package org.springframework.cloud.openfeign;

/**
 * HystrixFallbackHandler工厂
 *
 * @author wangwenhao
 * @date 2019年6月3日 上午9:39:52
 */
public interface HystrixFallbackHandlerFactory {

    /**
     * 创建handler
     *
     * @param feignClientClass
     * @param cause
     * @return
     */
    HystrixFallbackHandler createHandler(Class<?> feignClientClass, Throwable cause);

}