package vip.wangwenhao.mq.api.rpcservice;

import org.springframework.cloud.openfeign.DefaultHystrixFallbackFactory;
import org.springframework.cloud.openfeign.FallbackEnableFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.mq.api.dto.Message;

/**
 * @author wwh
 * @date 2020年01月20日 22:04
 */
@FeignClient(name = "service-mq", fallbackFactory = DefaultHystrixFallbackFactory.class)
public interface WebsocketRpcService extends FallbackEnableFeignClient {

    /**
     * 广播推送
     *
     * @param message
     * @return
     */
    @RequestMapping(value = "/mq/ws/topic", method = RequestMethod.POST)
    Result<Message> sendToTopic(@RequestBody Message message);

    /**
     * 点对点推送
     *
     * @param message
     * @return
     */
    @RequestMapping(value = "/mq/ws/user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result<Message> sendToUser(@RequestBody Message message);
}