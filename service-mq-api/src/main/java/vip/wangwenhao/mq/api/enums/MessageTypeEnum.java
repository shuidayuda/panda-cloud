package vip.wangwenhao.mq.api.enums;

/**
 * @author wwh
 * @date 2020年01月20日 21:50
 */

public enum MessageTypeEnum {

    /**
     * 用户其他设备登录
     */
    USER_OTHER_DEVICE_LOGIN(-1, "用户其他设备登录"),

    ;


    private Integer code;

    private String desc;

    MessageTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}