package vip.wangwenhao.mq.api.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author wwh
 * @date 2020年01月20日 14:10
 */

@Getter
@Setter
@ToString
public class Message<T> implements Serializable {

    /**
     * 用户唯一标识（sessionId）
     */
    private String uid;

    /**
     * 消息类型code
     */
    private Integer code;

    /**
     * 报文
     */
    private String content;

    /**
     * 数据
     */
    private T data;

    public Message() {
    }

    @Builder
    public Message(String uid, Integer code, String content, T data) {
        this.uid = uid;
        this.code = code;
        this.content = content;
        this.data = data;
    }

}