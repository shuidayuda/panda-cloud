package vip.wangwenhao.order.enums;

/**
 * @author wwh
 * @date 2020年05月19日 4:55 下午
 */

public enum OrderStatusEnum {

    /**
     *
     */
    INIT((byte)0,"初始化"),
    CREATED((byte)1,"已创建"),
    PAYED((byte)2,"已付款"),
    DELIVERY((byte)3,"已发货"),
    RECEIVED((byte)4,"已收货"),
    SUCCESS((byte)5,"交易完成"),
    CANCEL((byte)-2,"订单取消"),
    CLOSED((byte)-1,"订单关闭"),



    ;

    OrderStatusEnum(Byte code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private Byte code;
    private String desc;

    public Byte getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}