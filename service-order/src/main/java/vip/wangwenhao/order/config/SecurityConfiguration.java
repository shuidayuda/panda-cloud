package vip.wangwenhao.order.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import vip.wangwenhao.autoconfigure.constants.Oauth2Constants;
import vip.wangwenhao.autoconfigure.oauth2.AuthenticationTokenFilter;
import vip.wangwenhao.common.enums.RoleEnum;

@Configuration
@EnableWebSecurity
@EnableOAuth2Sso
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationTokenFilter authenticationTokenFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.logout().logoutSuccessUrl("http://localhost:8099/logout")
                .and()
                .authorizeRequests()
                .antMatchers("/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/api-doc/**").hasAuthority(RoleEnum.admin.name())
                .antMatchers("/order/login", "/error/**", "/static/**", "/hystrix.stream", "/favicon.ico").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .csrf()
                .disable()
                .headers().frameOptions().disable();
    }

    /**
     * 排除/hello路径拦截
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(Oauth2Constants.OAUTH_ALLOW_LINK_LIST);
    }
}