package vip.wangwenhao.order.config;

import com.alibaba.druid.pool.DruidDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import io.seata.spring.annotation.GlobalTransactionScanner;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import vip.wangwenhao.autoconfigure.annotations.EnableOauth2Client;
import vip.wangwenhao.autoconfigure.annotations.EnableRedisConfig;
import vip.wangwenhao.autoconfigure.base.WebMvcConfig;
import vip.wangwenhao.order.interceptors.InitInterceptor;

import javax.sql.DataSource;

/**
 * @author wwh
 * @date 2020年01月19日 22:33
 */

@Configuration
@EnableRedisConfig
@EnableOauth2Client
public class OrderConfig extends WebMvcConfig {

    @Autowired
    private InitInterceptor initInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //  基础拦截器
        registry.addInterceptor(initInterceptor)
                .addPathPatterns("/**");


        super.addInterceptors(registry);
    }
}