package vip.wangwenhao.order.entity;


/**
 * p_order_detail 实体类
 * @author wangwenhao
 */ 
public class OrderDetail{
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private Long orderId;

	/**
	 *  
	 */
	private Long productId;

	/**
	 *  
	 */
	private Integer productNum;

	public void setId(Long id){
		this.id = id;
	}

	public Long getId(){
		return this.id;
	}

	public void setOrderId(Long orderId){
		this.orderId = orderId;
	}

	public Long getOrderId(){
		return this.orderId;
	}

	public void setProductId(Long productId){
		this.productId = productId;
	}

	public Long getProductId(){
		return this.productId;
	}

	public void setProductNum(Integer productNum){
		this.productNum = productNum;
	}

	public Integer getProductNum(){
		return this.productNum;
	}

}

