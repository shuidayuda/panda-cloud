package vip.wangwenhao.order.entity;


/**
 * p_order 实体类
 * @author wangwenhao
 */ 
public class Order{
	/**
	 *  
	 */
	private Long id;

	/**
	 *  
	 */
	private String orderNum;

	/**
	 *  
	 */
	private Long totalAmount;

	/**
	 *  
	 */
	private Byte orderStatus;

	public void setId(Long id){
		this.id = id;
	}

	public Long getId(){
		return this.id;
	}

	public void setOrderNum(String orderNum){
		this.orderNum = orderNum;
	}

	public String getOrderNum(){
		return this.orderNum;
	}

	public void setTotalAmount(Long totalAmount){
		this.totalAmount = totalAmount;
	}

	public Long getTotalAmount(){
		return this.totalAmount;
	}

	public void setOrderStatus(Byte orderStatus){
		this.orderStatus = orderStatus;
	}

	public Byte getOrderStatus(){
		return this.orderStatus;
	}

}

