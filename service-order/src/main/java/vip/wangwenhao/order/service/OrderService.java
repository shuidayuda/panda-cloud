package vip.wangwenhao.order.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.order.dto.OrderDTO;

/**
 * @author wwh
 * @date 2020年05月14日 11:34 上午
 */

public interface OrderService {

    Result<OrderDTO> createOrder(BusinessActionContext businessActionContext, @BusinessActionContextParameter(paramName = "orderDTO")OrderDTO orderDTO);

}