package vip.wangwenhao.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vip.wangwenhao.common.exception.CommonException;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.common.util.BeanUtils;
import vip.wangwenhao.item.api.dto.ProductDTO;
import vip.wangwenhao.item.api.rpcservice.ProductRpcService;
import vip.wangwenhao.order.dto.OrderDTO;
import vip.wangwenhao.order.dto.OrderDetailDTO;
import vip.wangwenhao.order.entity.Order;
import vip.wangwenhao.order.entity.OrderDetail;
import vip.wangwenhao.order.enums.OrderStatusEnum;
import vip.wangwenhao.order.mapper.OrderDAO;
import vip.wangwenhao.order.mapper.OrderDetailDAO;
import vip.wangwenhao.order.service.OrderService;

import javax.annotation.Resource;
import java.util.List;

/**
*  
*
* @author wwh
* @date 2020年05月14日 11:42 上午
* 
*/

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDAO orderDAO;
    @Resource
    private OrderDetailDAO orderDetailDAO;
    @Resource
    private ProductRpcService productRpcService;

    @Override
    @GlobalTransactional
    public Result<OrderDTO> createOrder(BusinessActionContext businessActionContext,OrderDTO orderDTO) {
        log.warn("createOrder orderDTO:{}", JSONObject.toJSONString(orderDTO));
        //创建订单
        List<OrderDetailDTO> orderDetails = orderDTO.getDetailList();
        Order order = BeanUtils.copyTo(orderDTO, Order.class);
        order.setOrderNum(String.valueOf(System.currentTimeMillis()));
        long totalAmount = orderDetails.stream().mapToLong(orderDetail->{
            Result<ProductDTO> productDTOResult = productRpcService.queryById(orderDetail.getProductId());
            return productDTOResult.getData().getProductPrice();
        }).sum();
        order.setTotalAmount(totalAmount);
        order.setOrderStatus(OrderStatusEnum.INIT.getCode());
        orderDAO.insert(order);

        BeanUtils.copyProperties(order, orderDTO,false);

        orderDetails.stream().forEach(orderDetailDTO -> {
            orderDetailDTO.setOrderId(order.getId());
            OrderDetail orderDetail = BeanUtils.copyTo(orderDetailDTO, OrderDetail.class);
            orderDetailDAO.insert(orderDetail);
            orderDetailDTO.setId(orderDetail.getId());
        });
        //扣减库存
        orderDetails.stream().forEach(orderDetailDTO->{
            Result<Integer> integerResult = productRpcService.decrStock(orderDetailDTO.getProductId(), orderDetailDTO.getProductNum());
            log.info("integerResult:{}",JSONObject.toJSONString(integerResult));
            if (!integerResult.isSuccess() || integerResult.getData() <= 0){
                throw new CommonException("扣减库存失败！");
            }
        });

        order.setOrderStatus(OrderStatusEnum.CREATED.getCode());
        orderDAO.update(order);
        return Result.success(orderDTO);
    }
}