package vip.wangwenhao.order.interceptors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import vip.wangwenhao.common.constants.CommonConstants;
import vip.wangwenhao.common.util.CookieUtils;
import vip.wangwenhao.common.util.UUIDUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
*  
*
* @author wwh
* @date 2020年01月20日 23:03
* 
*/

@Component
public class InitInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uid = CookieUtils.getCookie(request, CommonConstants.UID_COOKIE_KEY);
        if (StringUtils.isBlank(uid)) {
            uid = UUIDUtils.getEMUUID();
            CookieUtils.setCookie(response, CommonConstants.UID_COOKIE_KEY, uid);
        }
        return super.preHandle(request, response, handler);
    }
}