package vip.wangwenhao.order.mapper;
import vip.wangwenhao.order.entity.Order;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface OrderDAO {
    
    int insert(Order order);

    int update(Order order);

    List<Order> query(Order order);

    int delete(Long id);

    Order selectByPrimaryKey(Long id);

}
