package vip.wangwenhao.order.mapper;
import vip.wangwenhao.order.entity.OrderDetail;
import java.util.List;

/**
*  
*
* @author wangwenhao
* 
*/
    
public interface OrderDetailDAO {
    
    int insert(OrderDetail orderDetail);

    int update(OrderDetail orderDetail);

    List<OrderDetail> query(OrderDetail orderDetail);

    int delete(Long id);

    OrderDetail selectByPrimaryKey(Long id);

}
