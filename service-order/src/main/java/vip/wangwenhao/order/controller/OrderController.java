package vip.wangwenhao.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.common.result.Result;
import vip.wangwenhao.order.dto.OrderDTO;
import vip.wangwenhao.order.service.OrderService;


/**
*  
*
* @author wwh
* @date 2020年05月14日 2:58 下午
* 
*/

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("create")
    public Result<OrderDTO> createOrder(@RequestBody OrderDTO orderDTO) {
        return orderService.createOrder(null,orderDTO);
    }

}