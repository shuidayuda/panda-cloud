package vip.wangwenhao.gateway.hystrix;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.wangwenhao.common.enums.ResultCode;
import vip.wangwenhao.common.result.Result;

/**
 * @author wangwenhao
 */
@RestController
public class FallbackController {

    /**
     * fallback
     *
     * @return
     */
    @RequestMapping("/fallback")
    public Result fallback() {
        return Result.fail(ResultCode.ROUTE_ERROR);
    }

}