package vip.wangwenhao.gateway.config;

import org.springframework.context.annotation.Configuration;
import vip.wangwenhao.autoconfigure.annotations.EnableRedisConfig;

/**
*  
*
* @author wwh
* @date 2020年03月10日 1:01 上午
* 
*/

@Configuration
@EnableRedisConfig
public class GatewayConfig {
}